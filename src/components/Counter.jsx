import React, { Component } from "react";

class Counter extends Component {
  state = {
    count: 1,
  };
  render() {
    return (
      <div className="m-2">
        <div>{this.getCount()}</div>
        <button className="btn btn-primary">Add</button>
      </div>
    );
  }

  getCount() {
    return this.state.count === 0 ? (
      <span className="badge bg-warning mb-2">Zero</span>
    ) : (
      this.state.count
    );
  }
}

export default Counter;
